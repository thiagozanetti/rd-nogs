# RD Nogs
Node Gathering Service

## How to use

First you need to install the dependencies:

```sh
npm install
```

And make sure you have `docker` and `docker-compose installed`

then type:

```sh
docker-compose up -d # to start up the MongoDB instance
npm start
```

The server will be listening on http://localhost:8080