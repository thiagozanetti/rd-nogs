import express from 'express'
import Contact from '../models/contact'

const router = express.Router()

router.post('/contacts', (req, res) => {
  Contact.ensureExists({ uid: req.body.uid }, req.body).then(
    (result) => res.send(result),
    (error) => res.send(error)
  )
})

router.get('/contacts', (req, res) => {
  Contact.find().then((result) => {
    res.send(result)
  })
})

export default router