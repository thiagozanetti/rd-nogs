import express from 'express'
import Visit from '../models/visit'
import Contact from '../models/contact'

const router = express.Router()

router.post('/visits', (req, res) => {
  const visit = new Visit(req.body)

  visit.save().then(
    (result) => Contact.ensureExists({ uid: result.uid }).then(() => res.send(result)),
    (error) => res.send(error)
  )
})

router.get('/visits', (req, res) => {
  Visit.find().then((result) => {
    res.send(result)
  })
})
router.get('/visits/:uid', (req, res) => {
  Visit.find({ uid: req.params.uid }).then((result) => {
    res.send(result)
  })
})

export default router