import visitsRouter from './visits'
import contactsRouter from './contacts'

const appRoutes = (app) => {
  app.use('/api/v1', [
    visitsRouter,
    contactsRouter,
  ])
}

export default {
  appRoutes
}