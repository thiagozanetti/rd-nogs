import db from './index'

// best way to check if an argument is an object
const isObject = (arg) => Object.prototype.toString.call(arg) === '[object Object]'

const ContactSchema = new db.mongoose.Schema({
  uid: {
    type: String,
    unique: true
  },
  name: {
    type: String,
    default: null
  },
  email: {
    type: String,
    default: null
  },
  when: {
    type: Date,
    default: null
  }
})

ContactSchema.static('ensureExists', function (condition, replacement, cb) {
  if (!isObject(condition)) {
    throw new TypeError(`Condition must be an object: got ${ typeof condition }`)
  }

  replacement = replacement || condition

  return this.update(condition, replacement, { upsert: true, setDefaultsOnInsert: true, new: true }, cb)
})

const Contact = db.mongoose.model('Contact', ContactSchema)

export default Contact