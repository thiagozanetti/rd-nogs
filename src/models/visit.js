import db from './index'

const VisitSchema = new db.mongoose.Schema({
  uid: String,
  from: String,
  to: String,
  when: Date,
})

const Visit = db.mongoose.model('Visit', VisitSchema)

export default Visit