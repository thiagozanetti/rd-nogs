import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost/nogs')

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection error:'))
db.once('open', () => console.log('DB connected!'))

export default {
  db,
  mongoose
}
  