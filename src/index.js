import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'

import router from './router'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

router.appRoutes(app)

app.listen(8080, () => {
  console.log('Listening on port 8080')
})